# CALCULADORA DA FREQUÊNCIA NATURAL DE POSTES

![](fnposte1.png)


Calculadora da frequência natural de postes de acordo com a norma **ANSI/TIA-222-H**.


![](fnposte2.png)

## Características do programa

- Programa executável para **Windows** gerado no software [Smath Studio](https://en.smath.com/view/SMathStudio/summary)..
- A calculadora pode ser utilizada em sistemas operacionais **GNU/Linux** via [WINE](https://www.winehq.org).
- Códido-fonte "**paper-like**" para [Smath Studio](https://en.smath.com/view/SMathStudio/summary).



## Uso

Sem necessidade de instalação, basta executar o arquivo **fnposte.exe** ou abrir no [Smath Studio](https://en.smath.com/view/SMathStudio/summary) o arquivo do código-fonte, **fnposte.sm**.
No programa executável basta preencher os dados conforme a sequência de abas disponíveis. 


## Licença


[GPLv3](http://www.gnu.org/licenses/)

[Clique aqui para ler a licença](license.txt)




